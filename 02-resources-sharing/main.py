# Variable set up
ResourcePools = [1,2,3,4,5,6,7,8,9,10]

TeamLead1Result = []
TeamLead2Result = []

# Input
print("Resources available: ", ResourcePools)
print("Please input your preference without a white space \nuse , as a separator \npress enter to input it")
TeamLead1Input = input("Team Lead 1: ")
TeamLead2Input = input("Team Lead 2: ")

# Process
TeamLead1Preferences = list(map(int, TeamLead1Input.split(",")))
TeamLead2Preferences = list(map(int, TeamLead2Input.split(",")))

ResourcePools.sort() # To handle any ResourcePools values
loop = len(ResourcePools)/2
for x in range(int(loop)): # self made round-robin algorithm
    if TeamLead1Preferences[x] in ResourcePools and TeamLead1Preferences[x] not in TeamLead2Result:
        TeamLead1Result.append(TeamLead1Preferences[x])
        ResourcePools.remove(TeamLead1Preferences[x])
        
    elif TeamLead1Preferences[x] not in ResourcePools and TeamLead1Preferences[x] in TeamLead2Result:
        TeamLead1Result.append(ResourcePools[0])
        ResourcePools.remove(ResourcePools[0])

    elif TeamLead1Preferences[x] not in ResourcePools and TeamLead1Preferences[x] not in TeamLead2Result and TeamLead1Preferences[x] in TeamLead1Result:
        TeamLead1Result.append(ResourcePools[0])
        ResourcePools.remove(ResourcePools[0])

    if TeamLead2Preferences[x] in ResourcePools and TeamLead2Preferences[x] not in TeamLead1Result:
        TeamLead2Result.append(TeamLead2Preferences[x])
        ResourcePools.remove(TeamLead2Preferences[x])

    elif TeamLead2Preferences[x] not in ResourcePools and TeamLead2Preferences[x] in TeamLead1Result:
        TeamLead2Result.append(ResourcePools[0])
        ResourcePools.remove(ResourcePools[0])

    elif TeamLead2Preferences[x] not in ResourcePools and TeamLead2Preferences[x] not in TeamLead1Result and TeamLead2Preferences[x] in TeamLead2Result:
        TeamLead2Result.append(ResourcePools[0])
        ResourcePools.remove(ResourcePools[0])

# Output
print(TeamLead1Result)
print(TeamLead2Result)