# Dkatalis Test

Please find the following solution for your questions in each folders

# Tech Stacks
- Python 3
- Terraform
- Alibaba Cloud Account

## Task 1
Just run the program with python 3 intepreter
```sh
python3 main.py
```

## Task 2
1. prepare your Alibaba cloud Access Key and Secret Key
2. copy the variables.tf.copy to variables.tf
```sh
cp variables.tf.copy variables.tf
```
change the required parameters

3. execute below command to provision the Wordpress infrastructures
```sh
terraform init
terraform validate
terraform plan
terraform apply
```
4. Enjoy your coffee. After everything is successful you will see the public IP that you have to visit to start configuring your Wordpress.


