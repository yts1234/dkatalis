# Provider configuration
terraform {
  required_providers {
    alicloud = {
      source  = "aliyun/alicloud"
      version = "1.152.0"
    }
  }
}

provider "alicloud" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

# user_data setup

locals {
  user_data_main = <<-EOF
#!/bin/bash
timedatectl set-timezone Asia/Jakarta
yum update -y
yum install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io
systemctl start docker
docker pull wordpress 
docker pull mariadb
docker network create wp_network
docker run --detach -p3306:3306 --network wp_network -v wp_db_vol:/var/lib/mysql --name wp_db --env MARIADB_USER=app_user --env MARIADB_PASSWORD=AppPassword12345 --env MARIADB_ROOT_PASSWORD=ApaHayo54321 -e MARIADB_DATABASE=wp_db mariadb
docker run --name wp_app --network wp_network -d -v wp_app_vol:/var/www/html -e WORDPRESS_DB_HOST=wp_db -e WORDPRESS_DB_USER=app_user -e WORDPRESS_DB_PASSWORD=AppPassword12345 -e WORDPRESS_DB_NAME=wp_db -p80:80 wordpress
EOF

  user_data_scale = <<-EOF
#!/bin/bash
timedatectl set-timezone Asia/Jakarta
yum update -y
yum install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io
systemctl start docker
docker pull wordpress 
docker run --name wp_app -d -v wp_app_vol:/var/www/html -e WORDPRESS_DB_HOST=192.168.1.2 -e WORDPRESS_DB_USER=app_user -e WORDPRESS_DB_PASSWORD=AppPassword12345 -e WORDPRESS_DB_NAME=wp_db -p80:80 wordpress
EOF
}

# Get Image data
data "alicloud_images" "centos" {
  most_recent = false
  name_regex  = "^centos_7_8*"
}

# Get zone data
data "alicloud_zones" "zone" {
  available_disk_category = "cloud_efficiency"
  available_instance_type = "ecs.t5-lc1m1.small"
}

# Security Group Provisioning
resource "alicloud_security_group" "group" {
  name                = "tf_sg_test"
  vpc_id              = alicloud_vpc.vpc.id
  inner_access_policy = "Accept"
}

# Security Group Rule
resource "alicloud_security_group_rule" "default" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "80/80"
  priority          = 1
  security_group_id = alicloud_security_group.group.id
  cidr_ip           = "0.0.0.0/0"
  description       = "Public access to webserver"
}

resource "alicloud_security_group_rule" "slb_access" {
  type              = "ingress"
  ip_protocol       = "all"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "-1/-1"
  priority          = 1
  security_group_id = alicloud_security_group.group.id
  cidr_ip           = "100.64.0.0/10"
  description       = "slb access"
}

# VPC Provisioning
resource "alicloud_vpc" "vpc" {
  vpc_name   = "tf_vpc_test"
  cidr_block = "192.168.0.0/16"
}

# Nat Gateway Provisioning
resource "alicloud_nat_gateway" "default" {
  vpc_id           = alicloud_vpc.vpc.id
  vswitch_id       = alicloud_vswitch.vswitch.id
  nat_gateway_name = "tf_nat_gateway_test"
  payment_type     = "PayAsYouGo"
  nat_type         = "Enhanced"
}

resource "alicloud_eip" "default" {
  bandwidth    = "1"
  payment_type = "PayAsYouGo"
  address_name = "tf_eip_test"
}

# Attach EIP to NAT Gateway
resource "alicloud_eip_association" "default" {
  allocation_id = alicloud_eip.default.id
  instance_id   = alicloud_nat_gateway.default.id
  instance_type = "Nat"
  force         = true
}

# SNAT rules for Internet Access
resource "alicloud_snat_entry" "default" {
  depends_on        = [alicloud_eip_association.default]
  snat_table_id     = alicloud_nat_gateway.default.snat_table_ids
  source_vswitch_id = alicloud_vswitch.vswitch.id
  snat_ip           = alicloud_eip.default.ip_address
}

# vSwitch Provisioning
resource "alicloud_vswitch" "vswitch" {
  vpc_id       = alicloud_vpc.vpc.id
  cidr_block   = "192.168.1.0/29"
  zone_id      = data.alicloud_zones.zone.zones[0].id
  vswitch_name = "tf_vswitch_application_test"
}

resource "alicloud_instance" "default" {
  availability_zone    = data.alicloud_zones.zone.ids.0
  security_groups      = alicloud_security_group.group.*.id
  instance_name        = "tf_wp_test"
  image_id             = data.alicloud_images.centos.images[0].id
  instance_type        = "ecs.t6-c1m1.large"
  instance_charge_type = "PostPaid"
  vswitch_id           = alicloud_vswitch.vswitch.id
  system_disk_category = "cloud_efficiency"
  system_disk_size     = 20
  user_data            = local.user_data_main
  private_ip           = "192.168.1.2"
  tags = {
    created = "terraform"
  }
}

# SLB Provisioning
resource "alicloud_slb_load_balancer" "default" {
  depends_on           = [alicloud_instance.default]
  load_balancer_name   = "tf_slb_test"
  load_balancer_spec   = "slb.s1.small"
  vswitch_id           = alicloud_vswitch.vswitch.id
  address_type         = "internet"
  internet_charge_type = "PayByTraffic"
}

# SLB Backend Server
resource "alicloud_slb_backend_server" "default" {
  depends_on       = [alicloud_instance.default]
  load_balancer_id = alicloud_slb_load_balancer.default.id


  backend_servers {
    server_id = alicloud_instance.default.id
    weight    = "100"
  }
}

# SLB Listener
resource "alicloud_slb_listener" "default" {
  depends_on                = [alicloud_instance.default, alicloud_slb_backend_server.default]
  load_balancer_id          = alicloud_slb_load_balancer.default.id
  backend_port              = 80
  frontend_port             = 80
  protocol                  = "http"
  bandwidth                 = 1
  sticky_session            = "on"
  sticky_session_type       = "insert"
  cookie_timeout            = 86400
  cookie                    = "tf_wp_application_cookie_test"
  health_check              = "on"
  health_check_connect_port = 80
  healthy_threshold         = 5
  unhealthy_threshold       = 5
  health_check_timeout      = 10
  health_check_interval     = 5
  health_check_http_code    = "http_2xx,http_3xx"
  scheduler                 = "rr"
  x_forwarded_for {
    retrive_slb_ip = true
    retrive_slb_id = true
  }
}

# Autoscaling Provisioning
resource "alicloud_ess_scaling_group" "default" {
  min_size           = 1
  max_size           = 2
  scaling_group_name = "tf_ess_test"
  default_cooldown   = 300
  vswitch_ids        = [alicloud_vswitch.vswitch.id]
  removal_policies   = ["OldestScalingConfiguration", "NewestInstance"]
  depends_on         = [alicloud_slb_listener.default]
  loadbalancer_ids   = [alicloud_slb_load_balancer.default.id]
}

# Autoscaling Configuration
resource "alicloud_ess_scaling_configuration" "default" {
  depends_on           = [alicloud_ess_scaling_group.default]
  instance_name        = "tf_ess_configuration_test"
  scaling_group_id     = alicloud_ess_scaling_group.default.id
  image_id             = data.alicloud_images.centos.ids.0
  instance_type        = "ecs.t6-c2m1.large"
  system_disk_category = "cloud_efficiency"
  system_disk_size     = 20
  security_group_id    = alicloud_security_group.group.id
  user_data            = local.user_data_scale
  force_delete         = true
  active               = true
  enable               = true
}

# Attach existing web server

resource "alicloud_ess_attachment" "default" {
  depends_on       = [alicloud_ess_scaling_configuration.default]
  scaling_group_id = alicloud_ess_scaling_group.default.id
  instance_ids     = [alicloud_instance.default.id]
  force            = true
}

# Autoscaling rule
resource "alicloud_ess_scaling_rule" "add_instance" {
  scaling_group_id  = alicloud_ess_scaling_group.default.id
  scaling_rule_name = "tf_wp_scaling_add_test"
  cooldown          = 120
  adjustment_type   = "TotalCapacity"
  adjustment_value  = 2
}

resource "alicloud_ess_scaling_rule" "remove_instance" {
  scaling_group_id  = alicloud_ess_scaling_group.default.id
  scaling_rule_name = "tf_wp_scaling_remove_test"
  cooldown          = 120
  adjustment_type   = "TotalCapacity"
  adjustment_value  = 1
}
# Autoscaling alarm
resource "alicloud_ess_alarm" "cpu_above_50" {
  name                = "tf_ess_alarm_cpu_above_50"
  alarm_actions       = [alicloud_ess_scaling_rule.add_instance.ari]
  scaling_group_id    = alicloud_ess_scaling_group.default.id
  metric_type         = "system"
  metric_name         = "CpuUtilization"
  period              = 60
  statistics          = "Average"
  threshold           = 70
  comparison_operator = ">="
  evaluation_count    = 1
}

resource "alicloud_ess_alarm" "cpu_below_50" {
  name                = "tf_ess_alarm_cpu_below_50"
  alarm_actions       = [alicloud_ess_scaling_rule.remove_instance.ari]
  scaling_group_id    = alicloud_ess_scaling_group.default.id
  metric_type         = "system"
  metric_name         = "CpuUtilization"
  period              = 60
  statistics          = "Average"
  threshold           = 70
  comparison_operator = "<"
  evaluation_count    = 1
}